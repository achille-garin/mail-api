# Simple Mail Forwarding API

This little program has been created to handle simple contact form submission. You can find in this README all infos on how to run it.

## Build the binary

Since the program is written in Go, you need to compile it for your execution platform. Before doing all the following steps, make sure your on the target machine or you have the same processor architecture. To go further in this guide you need Go installed on the machine. You can find how to install Go [here](https://golang.org/doc/install).

First of all, you need to clone this repository with the following command.

```bash
git clone https://git.garin.xyz/achille-garin/mail-api.git
```

Now you can enter in the created folder and build the source code.

```bash
cd mail-api
go build
``` 

By now you must have these files in your folder:

```
drwxr-xr-x    - user  1 Aug 13:16 .git
.rw-r--r--  690 user 16 Jul 18:37 main_test.go
.rw-r--r--  135 user 16 Jul 18:37 go.mod
.rw-r--r-- 3.0k user 16 Jul 18:37 main.go
.rw-r--r--   13 user 16 Jul 18:37 .gitignore
.rw-r--r--  396 user 16 Jul 18:37 go.sum
.rw-r--r--  131 user 28 Jul 23:02 .env.example
.rwxr-xr-x 7.3M user  1 Aug 13:31 mail-api
.rw-r--r--  837 user  1 Aug 14:01 README.md
```

If you do not have the *mail-api* binary you should troubleshoot the failed build.

## Configuration

The next step is to copy the *.env.example* file to same directory with a different name.

```bash
cp .env.example .env
```

Open the .env file and edit all variables inside it with your informations.

```conf
MAIL_SERVER=smtp.example.com
MAIL_PORT=587
MAIL_USERNAME=mymail@example.com
MAIL_PASSWORD=mypassword
MAIL_TO=recipient@example.com
```

The application need all these to work.

* **MAIL_SERVER** is the domain name of the smtp server you are going to use. For example if you want to use gmail to send emails you will need this domain *smtp.gmail.com*.
* **MAIL_PORT** is the port used by the smtp server, if you specified it wrong your email will not be accepted by the sever. The defaults ports for the secure smtp protocol are **587** and **465**. But it can be unencrypted as well, in this case it's more likely to be **25** (avoid unencrypted servers).
* **MAIL_USERNAME** is your identifier to your smtp provider. It can be your email address or another format.
* **MAIL_PASSWORD** is your password to your smtp provider.
* **MAIL_TO** is the email address which will be the sender of the message and both the recipient. This allow to just register your email in your mailbox.

## Usage

Once you are at this point, you need to copy the binary and the *.env* in the wanted directory. Then you can launch it. A webserver is start and listen on **port 8000**. It's actually the API running and listening at the root route of your domain. If you want to access it from the machine itself you must make a POST request at [](http://localhost:8000/).

The wanted format of the POST request is the following:

```json
{
    Contact: "johndoe@gmail.com",
    Subject: "Just want to make friends",
    Body: "Hello Mr Smith,\n I'm excited for the party that take place at Alma's home. I invite you to come"
}
```

As you can see, it's a really simple format and there is no authentification to the API so be aware of spamming. Every fields are required. And there is a regex check on the email address.

### Run on startup

This part is only for those who have **systemd** installed on their machines (most GNU / Linux distributions nowadays). So you first need to create a file named *mail-api.service* in your */etc/systemd/system/* folder. You probably need root access to do so. Then you put that text in the file :

```conf
[Unit]
Description=Golang mail forwarding api
[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/opt/golang-api/mail-api

[Install]
WantedBy=multi-user.target
```

In the ExecStart parameter you must put your own path to the binary. Avoid none root directories. Once the file is saved make sure the binary is not running or any other application that listen on port 8000. You can enter the followings command to activate the systemd service.

```bash
sudo systemctl deamon-reload
sudo systemctl start mail-api.service
sudo systemctl enable mail-api.service
```

Now the API is launched every time reboot or start your machine.
